#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/bootdevice/by-name/recovery:27141430:6a8bdc8d7234a41da4eea25f67b579e6cb9c868c; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/bootdevice/by-name/boot:25136434:c2fac3ccda7cd6e05df45b5580b583ba49f086b7 EMMC:/dev/block/bootdevice/by-name/recovery 6a8bdc8d7234a41da4eea25f67b579e6cb9c868c 27141430 c2fac3ccda7cd6e05df45b5580b583ba49f086b7:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
